package org.example;

import java.util.*;
import java.util.stream.Collectors;

public class App {

    public static void main(String[] args){
        List<String> splitArgs = Arrays.stream(args).collect(Collectors.toList());
        Map<Character, Integer> letters = new HashMap<>();
        for(String arg : splitArgs){
            arg = arg.toLowerCase(Locale.ROOT);
            for(char letter : arg.toCharArray()){
                if(!letters.containsKey(letter)){
                    letters.put(letter, 1);
                }else{
                    letters.put(letter, letters.get(letter) + 1);
                }
            }
        }

        letters.entrySet().stream()
                .sorted((o1, o2) -> {
                    if(o1.getKey() > o2.getKey()){
                        return 1;
                    }else if(o1.getKey() == o2.getKey()){
                        return 0;
                    }else{
                        return -1;
                    }
                })
                .forEach(entry -> System.out.println(entry.getKey() + " = " + entry.getValue()));


    }
}
